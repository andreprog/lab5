package test;

import static org.junit.Assert.*;
import main.FizzBuzz;
import main.FizzBuzzResult;

import org.junit.*;

public class TestFizzBuzz {
	FizzBuzz fizzBuzz;
	
	@Before
	public void createFizzBuzz() {
		fizzBuzz = new FizzBuzz();
	}
	
	private String printSequence(FizzBuzzResult result){
		return result.toString();
	}
	
	@Test
	public void testTwo() {
		FizzBuzzResult result = fizzBuzz.fizzbuzzSequence(2);
		assertEquals("Test two", "1 2", printSequence(result));
	}
	
	@Test
	public void testThree() {
		FizzBuzzResult result = fizzBuzz.fizzbuzzSequence(3);
		assertEquals("Test three", "1 2 fizz", printSequence(result));
	}
	
	@Test
	public void testFive() {
		FizzBuzzResult result = fizzBuzz.fizzbuzzSequence(5);
		assertEquals("Test five", "1 2 fizz 4 buzz", printSequence(result));
	
		result = fizzBuzz.fizzbuzzSequence(10);
		String resultString = printSequence(result);
		assertTrue("Test five", resultString.endsWith("fizz buzz"));
	}
	
	@Test
	public void testFifteen() {
		FizzBuzzResult result = fizzBuzz.fizzbuzzSequence(15);
		String resultString = printSequence(result);
		assertTrue("Test fifteen", resultString.endsWith("fizzbuzz"));
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testException() {
		fizzBuzz.fizzbuzzSequence(-5);
	}
	
	@Test
	public void testSeven() {
		FizzBuzzResult result = fizzBuzz.fizzbuzzSequence(7);
		String resultString = printSequence(result);
		assertTrue("Test seven", resultString.endsWith("woof"));
	}
	
	@Test
	public void testNewFizz() {
		FizzBuzzResult result = fizzBuzz.fizzbuzzSequence(35);
		String resultString = printSequence(result);
		assertTrue("Test new fizz", resultString.endsWith("fizzbuzzwoof"));
	}
	
	@Test
	public void testNewBuzz() {
		FizzBuzzResult result = fizzBuzz.fizzbuzzSequence(53);
		String resultString = printSequence(result);
		assertTrue("Test new buzz", resultString.endsWith("fizzbuzz"));
	}
	
	
}
