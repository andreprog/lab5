package main;

public class FizzBuzzResult {
	String sequence;
	
	public FizzBuzzResult(String s) {
		this.sequence = s;
	}
	
	public FizzBuzzResult(){
		sequence = "";
	}
	
	public void append(FizzBuzzResult inputSequence){
		manageEmptySequence();
		sequence += inputSequence.toString();
	}
	
	public void appendWithoutSpace(FizzBuzzResult inputSequence){
		sequence += inputSequence.toString();
	}
	
	private void manageEmptySequence(){
		if(sequence.length() != 0)
			sequence += " ";
	}
	
	public boolean isEmpty(){
		return sequence.length() == 0;
	}
	
	@Override
	public String toString() {
		return sequence;
	}
}
