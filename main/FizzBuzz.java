package main;

import java.util.LinkedHashMap;
import java.util.Map;

public class FizzBuzz {
	Map<Integer, String> nameAssociation = new LinkedHashMap<Integer, String>();
	
	public FizzBuzz() {
		createMap();
	}
	
	private void createMap() {
		nameAssociation.put(3, "fizz");
		nameAssociation.put(5, "buzz");
		nameAssociation.put(7, "woof");
	}
	
 	public FizzBuzzResult fizzbuzzSequence(int input) {
		
		checkInput(input);
		
		FizzBuzzResult result = new FizzBuzzResult();
		
		for(int i=1; i <= input; i++){
			result.append(nextSequence(i));
		}
		
		return result;
	}
	
 	private FizzBuzzResult nextSequence(int number){
		
		FizzBuzzResult appendedResult = new FizzBuzzResult();
		
		compose(appendedResult, number, 3);
			
		compose(appendedResult, number, 5);
		
		simpleCompose(appendedResult, number, 7);
		
		if(isNormalNumber(appendedResult)){
			return new FizzBuzzResult(String.valueOf(number));
		}
		
		return appendedResult;
		
	}
 	
 	private FizzBuzzResult simpleCompose(FizzBuzzResult result, int number, int divisor) {
		if(isDivisor(number, divisor)){
			String name = nameAssociation.get(divisor);
			result.appendWithoutSpace(new FizzBuzzResult(name)); 
			return result;
		}
		return new FizzBuzzResult(String.valueOf(number));
	}
 	
 	private FizzBuzzResult compose(FizzBuzzResult result, int number, int divisor) {
		if(isDivisorOrContained(number, divisor)){
			String name = nameAssociation.get(divisor);
			result.appendWithoutSpace(new FizzBuzzResult(name)); 
			return result;
		}
		return new FizzBuzzResult(String.valueOf(number));
	}
 	
	private void checkInput(int input){
		if(input < 0){
			throw new IllegalArgumentException("non accetto numeri negativi");
		}
	}
	
	private boolean isNormalNumber(FizzBuzzResult fizzBuzzResult){
		return fizzBuzzResult.isEmpty();
	}
	

	private boolean isDivisorOrContained(int number, int divisor){
		String divisorStringValue = String.valueOf(divisor);
		String numberStringValue = String.valueOf(number);
		return number % divisor == 0 ||  
				StringContainsNumber(numberStringValue, divisorStringValue);
	}
	
	private boolean StringContainsNumber(String entireString, String containedNumber){
		return entireString.contains(containedNumber);
	}
	
	private boolean isDivisor(int number, int divisor){
		return number % divisor == 0;
	}
}
